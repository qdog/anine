use crate::utils::*;
use scraper::{Html, Selector};

#[derive(Debug, Clone)]
struct DownloadEntry {
    title: String,
    magnet: String,
    size: String,
    up: u32,
    down: u32,
    completed: u32,
}

impl Aria2Downloadable for DownloadEntry {
    fn url(&self) -> &str {
        self.magnet.as_str()
    }

    fn name(&self) -> &str {
        self.title.as_str()
    }
}

impl RofiEntry for DownloadEntry {
    fn rofiname(&self) -> String {
        format!(
            "[🔺 {}][🔻 {}][✔️ {}][⚖️ {}] {}",
            self.up, self.down, self.completed, self.size, self.title
        )
    }
}

impl AnineEntry for DownloadEntry {}

impl DownloadEntry {
    pub fn new(
        title: String,
        magnet: String,
        size: String,
        up: u32,
        down: u32,
        completed: u32,
    ) -> Self {
        Self {
            title,
            magnet,
            size,
            up,
            down,
            completed,
        }
    }
}
pub struct Nyaasi {
    query: String,
}

impl Nyaasi {
    pub fn new(query: String) -> Self {
        Self { query }
    }
}

impl Anineable for Nyaasi {
    fn get_entries(&self) -> Vec<Box<dyn AnineEntry>> {
        let mut url = "https://nyaa.si/?c=1_2&q=".to_owned();
        url.push_str(&self.query);
        let res = reqwest::blocking::get(url).unwrap().text().unwrap();
        let parsed_html = Html::parse_document(&res);
        let tr_selector = Selector::parse("tbody > tr").unwrap();
        let title_selector = Selector::parse("td:nth-child(2) > a:not(.comments)").unwrap();
        let magnet_selector = Selector::parse("td:nth-child(3) > a:nth-child(2)").unwrap();
        let size_selector = Selector::parse("td:nth-child(4)").unwrap();
        let up_selector = Selector::parse("td:nth-child(6)").unwrap();
        let down_selector = Selector::parse("td:nth-child(7)").unwrap();
        let completed_selector = Selector::parse("td:nth-child(8)").unwrap();

        let mut titles: Vec<String> = Vec::new();
        let mut entries_list: Vec<Box<dyn AnineEntry>> = vec![];
        for element in parsed_html.select(&tr_selector) {
            let title = element.select(&title_selector).next().unwrap().inner_html();
            let magnet = element
                .select(&magnet_selector)
                .next()
                .unwrap()
                .value()
                .attr("href")
                .unwrap()
                .to_string();
            let size = element.select(&size_selector).next().unwrap().inner_html();
            let up = element.select(&up_selector).next().unwrap().inner_html();
            let down = element.select(&down_selector).next().unwrap().inner_html();
            let completed = element
                .select(&completed_selector)
                .next()
                .unwrap()
                .inner_html();
            let entry = DownloadEntry::new(
                title.clone(),
                magnet,
                size,
                up.parse().unwrap(),
                down.parse().unwrap(),
                completed.parse().unwrap(),
            );
            titles.push(title.clone());
            entries_list.push(Box::new(entry));
        }
        entries_list
    }
}
