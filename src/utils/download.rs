use aria2_ws::{response::Event, Client, TaskHooks, TaskOptions};
use core::time;
use futures::FutureExt;
use serde_json::Map;
use std::{
    collections::HashSet,
    sync::{Arc, Mutex},
    thread,
};
use tokio::spawn;

pub async fn example(host: &str, url: &str) {
    let client = Client::connect(&format!("ws://{}/jsonrpc", host), None)
        .await
        .unwrap();
    let mut extra_options = Map::new();
    extra_options.insert("seed-time".to_string(), 1.into());
    let options = TaskOptions {
        extra_options,
        // Add extra options which are not included in TaskOptions.
        ..Default::default()
    };

    // use `tokio::sync::Semaphore` to wait for all tasks to finish.
    let gid = client
        .add_uri(
            vec![url.to_string()],
            Some(options.clone()),
            None,
            Some(TaskHooks {
                on_complete: Some({
                    async move {
                        println!("finish task 1");
                    }
                    .boxed()
                }),
                on_error: Some({
                    async move {
                        println!("Task 1 error!");
                    }
                    .boxed()
                }),
            }),
        )
        .await
        .unwrap();
    println!("gid={:?}", gid);
    // Will 404

    let mut not = client.subscribe_notifications();

    let count = Arc::new(Mutex::new(0));
    let count2 = Arc::clone(&count);
    spawn(async move {
        let c = Arc::clone(&count);
        let mut gids = HashSet::new();
        loop {
            if let Ok(msg) = not.recv().await {
                println!("Received notification {:?}", &msg);
                match msg.event {
                    Event::Complete | Event::BtComplete | Event::Error => {
                        if !gids.contains(&msg.gid) {
                            gids.insert(msg.gid);
                            let mut num = c.lock().unwrap();
                            *num -= 1;
                        }
                    }
                    Event::Start => {
                        let mut num = c.lock().unwrap();
                        *num += 1;
                        println!("started");
                    }
                    _ => println!("other stuff"),
                }
            } else {
                return;
            }
        }
    });

    // Wait for 2 tasks to finish.
    let mut n_times_count_above_one = 0;
    loop {
        {
            let num = count2.lock().unwrap();
            if *num == 0 {
                n_times_count_above_one += 1;
            } else {
                n_times_count_above_one = 0;
            }
        }
        if n_times_count_above_one == 2 {
            break;
        }
        thread::sleep(time::Duration::from_secs(5));
    }

    // client.shutdown().await.unwrap();
}
