use std::process::Command;
mod sources;
mod utils;
use utils::*;

fn rofiselec(values: &[Box<dyn AnineEntry>]) -> usize {
    let rows: Vec<_> = values.iter().map(|x| x.rofiname()).collect();
    match rofi::Rofi::new(&rows).lines(10).run_index() {
        Ok(choice) => choice,
        Err(_) => panic!("error in rofi"),
    }
}

fn mpv(dresult: &DownloadResult) {
    let path2play = dresult.get_path();
    loop {
        let p = Command::new("mpv")
            .arg(path2play.to_str().unwrap())
            .arg("--pause")
            .status()
            .unwrap();
        println!("status = {:?}", p.code().unwrap());
        let code = p.code().unwrap();
        if code != 512 && code != 2 {
            break;
        }

        std::thread::sleep(std::time::Duration::from_secs(2));
    }
}

fn query<T: Anineable>(source: T) {
    let entries_list = source.get_entries();
    let selected = rofiselec(&entries_list);
    println!("{:?}", selected);
    let mut dresult = entries_list.get(selected).unwrap().downloadaria2();
    mpv(&dresult);
    println!("im leaving, downloading file = {:?}", dresult);
    dresult.stop();
}

fn main() {
    let mut args: Vec<String> = std::env::args().collect();
    args.remove(0);
    let source = sources::nyaasi::Nyaasi::new(args.join(" "));
    query(source);
}
