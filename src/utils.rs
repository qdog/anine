use std::env::temp_dir;
use std::net::UdpSocket;
use std::process::{Child, Command};
use tokio::runtime::Runtime;
mod download;

#[derive(Debug)]
pub struct DownloadResult {
    path: std::path::PathBuf,
    p: Child,
}

impl DownloadResult {
    pub fn get_path(&self) -> &std::path::PathBuf {
        &self.path
    }
    pub fn stop(&mut self) {
        self.p.kill().unwrap();
    }
}

pub trait Aria2Downloadable {
    fn url(&self) -> &str;
    fn name(&self) -> &str;
    fn downloadaria2(&self) -> DownloadResult {
        let tmpd = temp_dir().join("anine").join(self.name());
        std::fs::create_dir_all(&tmpd).unwrap();
        let socket = UdpSocket::bind("127.0.0.1:0").unwrap();
        let addr = socket.local_addr().unwrap();
        let p = Command::new("aria2c")
            .arg("--enable-rpc")
            .args(["--rpc-listen-port", &addr.port().to_string()])
            .args(["-d", tmpd.to_str().unwrap()])
            .spawn()
            .unwrap();
        let rt = Runtime::new().unwrap();
        let handle = rt.handle();
        handle.block_on(async {
            download::example(&addr.to_string(), self.url()).await;
        });
        // p.kill().unwrap();
        DownloadResult { path: tmpd, p }
    }
}

pub trait RofiEntry {
    fn rofiname(&self) -> String;
}

pub trait AnineEntry: RofiEntry + Aria2Downloadable {}

pub trait Anineable {
    fn get_entries(&self) -> Vec<Box<dyn AnineEntry>>;
}
